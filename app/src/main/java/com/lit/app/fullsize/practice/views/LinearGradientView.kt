package com.lit.app.fullsize.practice.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

class LinearGradientView : View {
    private val paint = Paint()
    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        paint.setShader(LinearGradient(0f, height.toFloat()/2, width.toFloat(),
            height.toFloat()/2, Color.RED, Color.GREEN, Shader.TileMode.REPEAT))
        canvas.drawRect(0f,0f, width.toFloat(), height.toFloat(), paint)
    }
}