package com.lit.app.fullsize.practice.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import com.lit.app.fullsize.practice.R

class DrawingView : View {

    var x1: Float = 0F
    var x2: Float = 0F
    var x3: Float = 0F

    var y1: Float = 0F
    var y2: Float = 0F
    var y3: Float = 0F
    var color: String? = null

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        val typearray: TypedArray =
            context.obtainStyledAttributes(attributeSet, R.styleable.DrawingView)
        x1 = typearray.getFloat(R.styleable.DrawingView_destinationX1, -1.0f)
        y1 = typearray.getFloat(R.styleable.DrawingView_destinationY1, -1.0f)
        x2 = typearray.getFloat(R.styleable.DrawingView_destinationX2, -1.0f)
        y2 = typearray.getFloat(R.styleable.DrawingView_destinationY2, -1.0f)
        x3 = typearray.getFloat(R.styleable.DrawingView_destinationX3, -1.0f)
        y3 = typearray.getFloat(R.styleable.DrawingView_destinationY3, -1.0f)
        color = "#ff0000"
        typearray.recycle()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val path = Path()
        val paint = Paint()
        paint.setColor(Color.parseColor(color))
        paint.style = Paint.Style.STROKE
        path.moveTo(x1, y1)
        path.quadTo(200f, 200f, x2, y2)
        path.quadTo(400f, 400f, x3, y3)
        canvas!!.drawPath(path, paint)
    }
}