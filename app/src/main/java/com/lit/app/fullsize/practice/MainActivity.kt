package com.lit.app.fullsize.practice

import android.graphics.drawable.ClipDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Gravity
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        val clipDrawable1 = ClipDrawable(
            (findViewById<ImageView>(R.id.image1).drawable as Drawable).getConstantState()!!.newDrawable(), Gravity.START,
            ClipDrawable.HORIZONTAL
        )
        val clipDrawable2 = ClipDrawable(
            (findViewById<ImageView>(R.id.image2).drawable as Drawable).getConstantState()!!.newDrawable(), Gravity.START,
            ClipDrawable.HORIZONTAL
        )
        Log.e("zhengzhou", clipDrawable1.toString())
        Log.e("zhengzhou cacheabsolute", this.cacheDir.absolutePath)
        Log.e("zhengzhou filesabsolute", this.filesDir.absolutePath)
        Log.e("zhengzhou envdatadirect", Environment.getDataDirectory().toString())
        Log.e("zhengzhou external", this.getExternalFilesDir("")!!.absolutePath.toString())
        Log.e("zhengzhou extecache", this.getExternalCacheDir()!!.absolutePath.toString())
        /*
2021-09-02 10:26:18.508 3294-3294/com.lit.app.fullsize.practice E/zhengzhou cacheabsolute: /data/user/0/com.lit.app.fullsize.practice/cache
2021-09-02 10:26:18.508 3294-3294/com.lit.app.fullsize.practice E/zhengzhou filesabsolute: /data/user/0/com.lit.app.fullsize.practice/files
2021-09-02 10:26:18.508 3294-3294/com.lit.app.fullsize.practice E/zhengzhou envdatadirect: /data
2021-09-02 10:26:18.514 3294-3294/com.lit.app.fullsize.practice E/zhengzhou external: /storage/emulated/0/Android/data/com.lit.app.fullsize.practice/files
2021-09-02 10:26:18.517 3294-3294/com.lit.app.fullsize.practice E/zhengzhou external cache: /storage/emulated/0/Android/data/com.lit.app.fullsize.practice/cache

         */
        findViewById<ImageView>(R.id.image1).setImageDrawable(clipDrawable1)
        findViewById<ImageView>(R.id.image2).setImageDrawable(clipDrawable2)
        findViewById<ImageView>(R.id.image1).setImageLevel(0)
        findViewById<ImageView>(R.id.image2).setImageLevel(10000)
        supportFragmentManager.beginTransaction().replace(android.R.id.content, TestFragment()).commit()
    }
}