package com.lit.app.fullsize.practice.views

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.TextView

@SuppressLint("AppCompatCustomView")
class ShimmerView : TextView {
    var dx = 0
    var gradient: LinearGradient? = null
    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        val paint = getPaint()
        val length = paint.measureText(text.toString())
        create(length)
        createGradient(length)
    }

    private fun create(length: Float) {
        val animator = ValueAnimator.ofInt(0, 2 *length.toInt())
        animator.addUpdateListener {
            dx = animator.animatedValue as Int
            postInvalidate()
        }
        animator.repeatMode = ValueAnimator.RESTART
        animator.repeatCount=ValueAnimator.INFINITE
        animator.duration=3000
        animator.start()
    }

    private fun createGradient(lengh: Float) {
        gradient = LinearGradient(-lengh, 0f, 0f, 0f,
            intArrayOf(Color.BLUE, Color.RED), floatArrayOf(
            0f, 1f), Shader.TileMode.CLAMP
        )
    }

    override fun onDraw(canvas: Canvas?) {
        val matrix = Matrix()
        matrix.setTranslate(dx.toFloat(), 0f)
        gradient?.setLocalMatrix(matrix)
        paint.shader = gradient
        super.onDraw(canvas)
    }
}