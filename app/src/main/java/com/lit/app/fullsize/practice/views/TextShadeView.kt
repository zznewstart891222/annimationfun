package com.lit.app.fullsize.practice.views

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

class TextShadeView: View {
    private val paint = Paint()
    constructor(context: Context, attrs: AttributeSet): super(context, attrs) {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val colors = intArrayOf(Color.RED, Color.GREEN, Color.BLUE, Color.GRAY, Color.BLACK)
        val pos = floatArrayOf(0f, 0.2f, 0.4f, 0.6f, 1.0f)
        val gradient = LinearGradient(0F, 0F ,
            width.toFloat()/2, height.toFloat()/2, colors, pos, Shader.TileMode.MIRROR)
        paint.setShader(gradient)
        paint.textSize = 50F
        canvas.drawText("ZZ is smart",0f , 200f, paint)
    }

}