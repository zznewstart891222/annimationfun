package com.lit.app.fullsize.practice.views

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator

class WaterView : View {
    private var path: android.graphics.Path = Path()
    var paint: android.graphics.Paint = Paint()
    val wl: Int = 1200
    var dx = 0

    constructor(context: Context, attributeSet: AttributeSet) :
            super(context, attributeSet) {
        paint = Paint()
        paint.setColor(Color.GREEN)
        paint.style = Paint.Style.FILL
        startAnimation()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        path.reset()
        val originY = 300f
        val hf = wl / 2
        path.moveTo(-wl.toFloat() + dx, originY)
        for (k in (-wl)..(width + wl) step wl) {
            path.rQuadTo(hf.toFloat() / 2, -100f, hf.toFloat(), 0f)
            path.rQuadTo(hf.toFloat() / 2, 100f, hf.toFloat(), 0f)
        }
        path.lineTo(width.toFloat(), height.toFloat())
        path.lineTo(0f, height.toFloat())
        path.close()
        canvas.drawPath(path, paint)
    }

    fun startAnimation() {
        val animator: ValueAnimator = ValueAnimator.ofInt(0, wl)
        animator.duration = 1000
        animator.repeatCount = ValueAnimator.INFINITE
        animator.interpolator = LinearInterpolator()
        animator.addUpdateListener {
            dx = animator.getAnimatedValue() as Int
            invalidate()
        }
        animator.start()
    }
}